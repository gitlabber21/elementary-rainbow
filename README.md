# Rainbow (logotype-mesh) Plymouth theme for elementary OS  
_As seen on https://imgur.com/gallery/yJIKGnD_  
_By u/MrKotlet on Reddit, inspired by u/1280px's "edwD1" wallpaper post_

## Install instructions and troubleshooting

Setting plymouth themes in general can be a bit finnicky, and the default theme was quite persistent for me, so you might have to play around with it a little bit, and google around. It is assumed you do the following at own risk - since root commands and directories are used, I am not responsible if something goes wrong.

Begin by downloading, extracting and copying the entire elementary-rainbow folder (directory) to /usr/share/plymouth/themes. You can open a file manager window as administrator (right-click Files icon on your taskbar) and copy it over that way, or open a terminal and do:

    cd /home/your/downloads

    sudo cp elementary-rainbow /usr/share/plymouth/themes/elementary-rainbow

Make sure you don't have folders inside of folders. It should just be /usr/share/plymouth/themes/elementary-rainbow/{theme files}

Then, all you should need to do is:

    sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/elementary-rainbow/elementary-rainbow.plymouth 100

And to confirm

    sudo update-alternatives --config default.plymouth

Followed by

    sudo update-initramfs -ukall

If that doesn't work, try opening files as administrator (see above) and remove the default.plymouth file from /usr/share/plymouth/themes/, copy as link (Ctrl+Shift+C) the /usr/share/plymouth/themes/elementary-rainbow/elementary-rainbow.plymouth file, then go back into /usr/share/plymouth/themes/ and paste (Ctrl+V) it there, and rename it to "default.plymouth". Then redo the last 3 commands.
